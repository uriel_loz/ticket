<!-- TODO: remove header for no repeat -->
<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<title>Report</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link href="{{ asset( 'css/bootstrap.min.css' ) }}" rel="stylesheet" />

		<style>
			table thead {
				display: table-row-group;
			}

			tfoot {
				display: table-row-group;
			}

			tr {
				page-break-inside: avoid;
			}

			body {
				font-size: 12px;
			}

			.table td, .table th {
				padding: .20rem;
			}

			#main {
				margin: 0 auto;
				width: 50%;
			}

			#logo {
				max-width: 30%;
			}

			.page-break {
				page-break-after: always;
			}
		</style>
	</head>
	<body>
		

		<div class="text-center" style="height: 200px;">
			<img id="logo" src="{{ asset( 'img/logo.png' ) }}" alt="" />
		</div>

		<br />

		
		<table id="main" class="table table-bordered">
			<thead class="thead-dark">
				<tr>
					<th width="30%">Concepto</th>
					<th width="30%">Subconcepto</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Fondo Inicial</td>
					<td></td>
					<td class="text-right table-active">$ </td>
				</tr>
				<tr>
					<td>Total AMEX</td>
					<td></td>
					<td class="text-right">$ </td>
				</tr>
				<tr>
					<td>Total TDC y TDD</td>
					<td></td>
					<td class="text-right">$ </td>
				</tr>
				<tr>
					<td>Total Efectivo</td>
					<td></td>
					<td class="text-right">$ </td>
				</tr>
				<tr>
					<td></td>
					<td>Recibido</td>
					<td class="text-right">$ </td>
				</tr>
				<tr>
					<td></td>
					<td>Cambio</td>
					<td class="text-right">$ </td>
				</tr>
				<tr>
					<td>Pagos de Servicio</td>
					<td></td>
					<td class="text-right table-active">$ </td>
				</tr>
				<tr>
					<td>Retiros</td>
					<td></td>
					<td class="text-right table-active">$ </td>
				</tr>
				<tr>
					<td>Total Ventas</td>
					<td></td>
					<td class="text-right table-active">$ </td>
				</tr>
				<!-- <tr>
					<td></td>
					<td></td>
					<td></td>
				</tr> -->
				<tr>
					<td><strong>Total en Caja</strong></td>
					<td></td>
					<td class="text-right table-active">$ </td>
				</tr>
			</tbody>
		</table>

		<div class="page-break"></div>

		<h2 class="text-center"></h2>
{{-- 
		@foreach( as $key => $payment )
			<table class="table table-striped table-bordered">
				<caption># {{ $key + 1 }}</caption>
				<thead class="thead-dark">
					<tr>
						<th width="8%">SKU</th>
						<th>Concepto</th>
						<th width="15%">P Unitario</th>
						<th width="20%">Cantidad</th>
						<th width="18%">Total x Concepto</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $payment[ 'paymentProducts' ] as $product )
						<tr>
							<td>{{ $product -> product -> sku }}</td>
							<td>{{ $product -> product -> name }}</td>
							<td class="text-right">$</td>
							<td class="text-center">{{ $product -> count }}</td>
							<td class="text-right">$ {{ $product -> total }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3"></td>
						<td><strong>Subtotal</strong></td>
						<td class="text-right"><strong>$ {{  1.16, }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>I.V.A.</strong></td>
						<td class="text-right"><strong>$ {{  ( $payment -> total / 1.16 ), }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Total</strong></td>
						<td class="text-right table-active"><strong>$ </strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Recibo #</strong></td>
						<td class="text-right"><strong>{{ $payment -> id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong># Pedido MMS</strong></td>
						<td class="text-right"><strong>{{ $payment -> mms_order_id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Fecha y hora</strong></td>
						<td class="text-right"><strong>{{ $payment -> created_at }}</strong></td>
					</tr>
				</tfoot>
			</table>
		@endforeach --}}

		<div class="page-break"></div>

		<h2 class="text-center"></h2>

		{{-- @foreach(  -> amex as $key => $payment )
			<table class="table table-striped table-bordered">
				<caption># {{ $key + 1 }}</caption>
				<thead class="thead-dark">
					<tr>
						<th width="8%">SKU</th>
						<th>Concepto</th>
						<th width="15%">P Unitario</th>
						<th width="20%">Cantidad</th>
						<th width="18%">Total x Concepto</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $payment[ 'paymentProducts' ] as $product )
						<tr>
							<td>{{ $product -> product -> sku }}</td>
							<td>{{ $product -> product -> name }}</td>
							<td class="text-right">$</td>
							<td class="text-center">{{ $product -> count }}</td>
							<td class="text-right">$ {{ $product -> total }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3"></td>
						<td><strong>Subtotal</strong></td>
						<td class="text-right"><strong>$ {{  1.16, }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>I.V.A.</strong></td>
						<td class="text-right"><strong>$ {{  ( $payment -> total / 1.16 ), }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Total</strong></td>
						<td class="text-right table-active"><strong>$ </strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Recibo #</strong></td>
						<td class="text-right"><strong>{{ $payment -> id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong># Pedido MMS</strong></td>
						<td class="text-right"><strong>{{ $payment -> mms_order_id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Fecha y hora</strong></td>
						<td class="text-right"><strong>{{ $payment -> created_at }}</strong></td>
					</tr>
				</tfoot>
			</table>
		@endforeach --}}

		<div class="page-break"></div>

		<h2 class="text-center"></h2>

		{{-- @foreach(  -> cash as $key => $payment )
			<table class="table table-striped table-bordered">
				<caption># {{ $key + 1 }}</caption>
				<thead class="thead-dark">
					<tr>
						<th width="8%">SKU</th>
						<th>Concepto</th>
						<th width="15%">P Unitario</th>
						<th width="20%">Cantidad</th>
						<th width="18%">Total x Concepto</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $payment[ 'paymentProducts' ] as $product )
						<tr>
							<td>{{ $product -> product -> sku }}</td>
							<td>{{ $product -> product -> name }}</td>
							<td class="text-right">$</td>
							<td class="text-center">{{ $product -> count }}</td>
							<td class="text-right">$ {{ $product -> total }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3"></td>
						<td><strong>Subtotal</strong></td>
						<td class="text-right"><strong>$ {{  1.16, }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>I.V.A.</strong></td>
						<td class="text-right"><strong>$ {{  ( $payment -> total / 1.16 ), }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Total</strong></td>
						<td class="text-right table-active"><strong>$ </strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Recibo #</strong></td>
						<td class="text-right"><strong>{{ $payment -> id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong># Pedido MMS</strong></td>
						<td class="text-right"><strong>{{ $payment -> mms_order_id }}</strong></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td><strong>Fecha y hora</strong></td>
						<td class="text-right"><strong>{{ $payment -> created_at }}</strong></td>
					</tr>
				</tfoot>
			</table>
		@endforeach --}}

		<div class="page-break"></div>

		<h2 class="text-center"></h2>

		<table class="table table-striped table-bordered">
			<thead class="thead-dark">
				<tr>
					<th width="8%">ID</th>
					<th>Descripción</th>
					<th width="15%">Fecha y hora</th>
					<th width="20%">Cantidad</th>
				</tr>
			</thead>
			<tbody>
				{{-- @foreach(  -> partial as $key => $box )
					<tr>
						<td>{{ $box -> id }}</td>
						<td>{{ $box -> description }}</td>
						<td>{{ $box -> created_at }}</td>
						<td class="text-right">$</td>
					</tr>
				@endforeach --}}
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"></td>
					<td><strong>Total</strong></td>
					<td class="text-right table-active">$ </td>
				</tr>
			</tfoot>
		</table>

		<div class="page-break"></div>

		<h2 class="text-center"></h2>

		<table class="table table-striped table-bordered">
			<thead class="thead-dark">
				<tr>
					<th width="8%">ID</th>
					<th>Descripción</th>
					<th width="15%">Fecha y hora</th>
					<th width="20%">Cantidad</th>
				</tr>
			</thead>
			<tbody>
				{{-- @foreach(  -> service as $key => $box )
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right">$</td>
					</tr>
				@endforeach --}}
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"></td>
					<td><strong>Total</strong></td>
					<td class="text-right table-active">$ </td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>