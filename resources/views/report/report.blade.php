
<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<title>Report</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<style>
			{{ include( public_path().'/css/bootstrap.min.css' ) }}

			table thead {
				display: table-row-group;
			}

			tfoot {
				display: table-row-group;
			}

			tr {
				page-break-inside: avoid;
			}

			body {
				font-size: 12px;
			}

			.table td, .table th {
				padding: .20rem;
			}

			#main {
				margin: 0 auto;
				/* width: 50%; */
			}

			#logo {
				max-width: 60%;
			}

			.page-break {
				page-break-after: always;
			}
		</style>
		
	</head>
	<body>
		<h2 class="text-center">Reporte de Reservas Faltantes de Pagar</h2>
	
		<h5 class="text-center">Suma total</h5>
		<table id="main" class="table table-bordered">
			<thead class="thead-dark">
				<tr>
					<th width="30%">Mes</th>
					<th width="30%">Total de reservas</th>
					<th width="30%">Monto aproximado total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Enero</td>
					<td class="text-right">33</td>
					<td class="text-right">$2,075.00</td>
				</tr>
				<tr>
					<td>Febrero</td>
					<td class="text-right">29</td>
					<td class="text-right">$1,550.00</td>
				</tr>
				<tr>
					<td>Marzo</td>
					<td class="text-right">63</td>
					<td class="text-right">$3,460.00</td>
				</tr>
				<tr>
					<td>Abril</td>
					<td class="text-right">190</td>
					<td class="text-right">$11,340.00</td>
				</tr>
				<tr>
					<td>Mayo</td>
					<td class="text-right">885</td>
					<td class="text-right">$61,455.00</td>
				</tr>
				<tr>
					<td>Junio</td>
					<td class="text-right">959</td>
					<td class="text-right">$66,440.00</td>
				</tr>
				<tr>
					<td>Julio</td>
					<td class="text-right">389</td>
					<td class="text-right">$25,220.00</td>
				</tr>
				<tr>
					<td>Agosto</td>
					<td class="text-right">524</td>
					<td class="text-right">$33,175.00</td>
				</tr>
				<tr>
					<td>Septiembre</td>
					<td class="text-right">1237</td>
					<td class="text-right">$81,590.00</td>
				</tr>
				<tr>
					<td>Octubre</td>
					<td class="text-right">1462</td>
					<td class="text-right">$101,180.00</td>
				</tr>
				<tr>
					<td>Noviembre</td>
					<td class="text-right">1273</td>
					<td class="text-right">$89,415.00</td>
				</tr>
				<tr>
					<td>Diciembre</td>
					<td class="text-right">905</td>
					<td class="text-right">$67,575.00</td>
				</tr>
				<tr>
					<td><strong>Total</strong></td>
					<td class="text-right table-active">7949</td>
					<td class="text-right table-active">$544,475.00</td>
				</tr>
			</tbody>
		</table>
		
		<hr />


		<!-- Status 5 -->

		<br />
        <h2 class="text-center">Reporte de Reservas Pagadas</h2>
		<h5 class="text-center">Suma total</h5>
		<table id="main" class="table table-bordered">
			<thead class="thead-dark">
				<tr>
					<th width="30%">Mes</th>
					<th width="30%">Total de reservas</th>
					<th width="30%">Monto aproximado total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Enero</td>
					<td class="text-right">551</td>
					<td class="text-right">$42,545.00</td>
				</tr>
				<tr>
					<td>Febrero</td>
					<td class="text-right">358</td>
					<td class="text-right">$21,680.00</td>
				</tr>
				<tr>
					<td>Marzo</td>
					<td class="text-right">359</td>
					<td class="text-right">$20,655.00</td>
				</tr>
				<tr>
					<td>Abril</td>
					<td class="text-right">279</td>
					<td class="text-right">$14,790.00</td>
				</tr>
				<tr>
					<td>Mayo</td>
					<td class="text-right">518</td>
					<td class="text-right">$20,345.00</td>
				</tr>
				<tr>
					<td>Junio</td>
					<td class="text-right">299</td>
					<td class="text-right">$12,670.00</td>
				</tr>
				<tr>
					<td>Julio</td>
					<td class="text-right">332</td>
					<td class="text-right">$18,380.00</td>
				</tr>
				<tr>
					<td>Agosto</td>
					<td class="text-right">279</td>
					<td class="text-right">$14,140.00</td>
				</tr>
				<tr>
					<td>Septiembre</td>
					<td class="text-right">701</td>
					<td class="text-right">$21,040.00</td>
				</tr>
				<tr>
					<td>Octubre</td>
					<td class="text-right">646</td>
					<td class="text-right">$16,135.00</td>
				</tr>
				<tr>
					<td>Noviembre</td>
					<td class="text-right">332</td>
					<td class="text-right">$13,625.00</td>
				</tr>
				<tr>
					<td>Diciembre</td>
					<td class="text-right">122</td>
					<td class="text-right">$3,750.00</td>
				</tr>
				<tr>
					<td><strong>Total</strong></td>
					<td class="text-right table-active">4776</td>
					<td class="text-right table-active">$219,755.00</td>
				</tr>
			</tbody>
		</table>
		
		<hr />

		<br /><br /><br /><br />
	</body>
</html>