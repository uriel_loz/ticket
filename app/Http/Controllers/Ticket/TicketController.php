<?php

namespace App\Http\Controllers\Ticket;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function show()
    {

        $pdf = PDF::loadView( 'report.report' )->setPaper( 'letter' );
        $date = Carbon::toDateString();
        // return $pdf->download( 'report.pdf' );

        $path = public_path( 'storage/report' );
        $fileName =  'report-' . "$date"  . '.' . 'pdf' ;
        $pdf->save( $path . '/' . $fileName );
        return $pdf->download( $fileName );
    }
}
